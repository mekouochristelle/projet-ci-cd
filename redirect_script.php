<?php
// Réaliser toute action nécessaire avant la redirection, si nécessaire

// Rediriger vers app_v2
header("Location: http://app_v2/");

// Assurer que le script se termine ici
exit;
?>
