<?php
// authentification.php

// Démarre la session
//session_start();

// Tableau d'utilisateurs (à remplacer par une base de données en production)
$utilisateurs = [
    'sandra' => '1234',
    'andre' => '1234',
    // Ajoutez d'autres utilisateurs au besoin
];

// Fonction de vérification de l'authentification
function authentifier($utilisateur, $motdepasse, $utilisateurs)
{
    // Vérifie si l'utilisateur existe dans le tableau et si le mot de passe correspond
    if (isset($utilisateurs[$utilisateur]) && $utilisateurs[$utilisateur] === $motdepasse) {
        // Stocke le nom d'utilisateur dans la session
        $_SESSION['utilisateur'] = $utilisateur;
        return true;
    }
    return false;
}
?>






