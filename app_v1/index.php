<?php
// Inclure le fichier d'authentification
include 'auth.php';
// Traitement du formulaire
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $utilisateur = isset($_POST['utilisateur']) ? $_POST['utilisateur'] : '';
    $motdepasse = isset($_POST['motdepasse']) ? $_POST['motdepasse'] : '';
    

    // Vérification de l'authentification en utilisant la fonction du fichier authentification.php
    if (authentifier($utilisateur, $motdepasse, $utilisateurs)) {
        // Démarrer la session
        //session_start();

        // Stocke le nom d'utilisateur dans la session
        $_SESSION['utilisateur'] = $utilisateur;

        /*if ($utilisateur == 'sandra') {
            setcookie('user', 'sandra', time() + 3600, '/');
        }

        // Ajouter un cookie pour l'utilisateur B
        if ($utilisateur == 'andre') {
            setcookie('user', 'andre', time() + 3600, '/');
        }*/

        // Redirige l'utilisateur vers une page sécurisée
        include 'bienvenu.php';
        afficherMessageBienvenue($utilisateur);
        exit();
    } else {
        echo 'Identifiants incorrects. Veuillez réessayer.';
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Formulaire de Connexion</title>
</head>
<body>
    <section>
    <h1>Connexion</h1>
    <form method="post" action="">
        <label for="utilisateur">Utilisateur</label>
        <input type="text" id="utilisateur" name="utilisateur" required>
        <br>
        <label for="motdepasse">Mot de passe</label>
        <input type="password" id="motdepasse" name="motdepasse" required>
        <br>
        <input type="submit" value="Se connecter">
    </form>
    </section>
</body>
</html>